import logo from "./logo.svg";
import "./App.css";
import ColorBox from "./Components/ColorBox";

function App() {
	const name = "Pham Viet Cuong";
	const age = 18;
	const isMale = true;
	const student = {
		name: "Vietcuong Pham",
		age: 25,
	};
	const colorList = ["red", "blue", "green"];

	let i = 0;
	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />

				<p>
					Hello {name} - {age} - {isMale ? "Male" : "Female"}
				</p>
				{isMale ? <p>Male</p> : <p>Female</p>}
				{isMale && (
					<>
						<p>Male 1</p>
						<p>Male 2</p>
						<p>Male 3</p>
					</>
				)}
				{!isMale && <p>Female</p>}

				<p>{student.name}</p>
				<div style={{ display: "flex" }}>
					{colorList.map((color) => {
						i++;
						return (
							<>
								<p>{color + i}</p>
								<ColorBox color={color} />
							</>
						);
					})}
				</div>
			</header>
		</div>
	);
}

export default App;
